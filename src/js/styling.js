var sections = ['#Studies','#Skills','#Works'];
var skillLogos = ['bash', 'cpp', 'csharp', 'css3', 'england', 'haskell', 'html5', 'java', 'javascript', 'powershell', 'python', 'windows', 'auto'];
var workLogos = ['mcdonalds', 'ON', 'logis'];

$(document).ready(function(){
    $('.nav-item').on('click', function (event) {
        var target = $(this.getAttribute('href'));
        if (target.length) {
            event.preventDefault();
            var navbarH=$('#nav').height();
            var shouldClick=false;
            if (150 < navbarH){
                shouldClick = true;
            }
            var scrollToPosition = $(target).offset().top - navbarH;
            $('html, body').stop().animate({
                scrollTop: scrollToPosition
            }, 1000);
        }
        if(shouldClick){
            $('#togglerButton').trigger("click");
        }
    });
    var delayInMilliseconds = 1000; //1 second

    setTimeout(function () {
        if ($('#Studies').width() > 906) {
            $('.decoration').show();
            //makeCirclesInWithImgFrom('#Skills div.line', skillLogos);
            //makeCirclesInWithImgFrom('#Works div.line', workLogos);
            for (var i = 0; i < sections.length; i++) {
                makeCirclesIn(sections[i] + ' div.line');
            }
        } else {
            $('.decoration').hide();
        }
    }, delayInMilliseconds);
    
    $(window).resize(function () {
        $('.createddecorationcircle').remove();
        if ($('#Studies').width() > 906) {
            $('.decoration').show();
            //makeCirclesInWithImgFrom('#Skills div.line', skillLogos);
           // makeCirclesInWithImgFrom('#Works div.line', workLogos);
            for (var i = 0; i < sections.length; i++) {
                makeCirclesIn(sections[i] + ' div.line');
            }
        } else {
            $('.decoration').hide();
        }
    });
});

function makeCirclesIn(elem){
    var numOfCirles = Math.floor(Math.random() * ($(elem).height()/20));
    numOfCirles += $(elem).height()/50;
    for(var i=0; i<numOfCirles; i++){
        var hw = Math.floor(Math.random() * 7);
        hw+=1;
        var fromtop = Math.floor(Math.random() * $(elem).height());
        var margin = Math.floor(Math.random() * 160);
        margin+=30;
        $(elem).append('<div class="rounded-circle decoration createddecorationcircle" style="width:'+hw+'px; height:'+hw+'px; top:'+fromtop+'px; margin-right: -'+margin+'px;"></div>');
    }
}
function makeCirclesInWithImgFrom(elem, listOfLogos){
    for (var i = 0; i < listOfLogos.length; i++) {
        var hw = Math.floor(Math.random() * 20);
        hw += 20;
        var fromtop = Math.floor(Math.random() * ($(elem).height()-20));
        var margin = Math.floor(Math.random() * 110);
        margin += 55;
        $(elem).append('<div class="rounded-circle decoration createddecorationcircle" style="width:' + hw + 'px; height:' + hw + 'px; top:' + fromtop + 'px; margin-right: -' + margin + 'px; z-index:100;"><img src="kepek/' + listOfLogos[i] + '.png" alt="" width="'+(hw-5)+'"></div>');
    }
}

//index goes from 1 to 3
function changeProfileTo(index){
    let profil = $('#selfPortrait img');
    if(0<index && index < numberOfProfiles+1){
        profil.attr('src', `kepek/onarckep${index}.jpg`);
    }
}

/*let profileCount = 1;
let numberOfProfiles = 4;
window.onload = window.setInterval(
function(){
    profileCount = (profileCount > numberOfProfiles) ? 1 : profileCount+1;
    changeProfileTo(profileCount);
} ,5000);*/