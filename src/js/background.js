"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
class Background {
    constructor() {
        this.textures = {};
        const textureloader = new THREE.TextureLoader();
        let ref = '.';
        if (window.location.pathname.includes('en') || window.location.pathname.includes('de')) {
            ref = '..';
        }
        this.textures.metal = textureloader.load(ref + '/kepek/textures/metal_mat.png');
        this.canvas = document.querySelector("#background-canvas");
        this.scene = new THREE.Scene();
        this.camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.renderer = new THREE.WebGLRenderer({
            canvas: this.canvas
        });
        this.light = new THREE.AmbientLight(0xffffff);
        this.light.position.set(5, 5, 5);
        this.light.intensity = 3;
        this.spotLight = new THREE.SpotLight(0xffffffff);
        this.spotLight.position.set(0, 0, 30);
        this.spotLight.intensity = 3;
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.camera.position.setZ(0);
        this.renderer.render(this.scene, this.camera);
        window.addEventListener('resize', () => {
            while (this.scene.children.length > 0) {
                this.scene.remove(this.scene.children[0]);
            }
            this.update();
            this.start();
        });
        document.addEventListener('scroll', () => {
            const t = document.body.getBoundingClientRect().top;
            this.camera.position.z = t * -0.01;
            this.camera.position.x = t * -0.0002;
            this.camera.position.y = t * -0.0002;
            this.update();
        });
    }
    start() {
        this.scene.add(this.light);
        this.scene.add(this.spotLight);
        this.spaceShip();
        for (let i = 0; i < 500; i++) {
            const geometry = new THREE.SphereGeometry(0.3, 4, 4);
            const material = new THREE.MeshBasicMaterial({ color: 0xF99B2A, wireframe: true });
            const star = new THREE.Mesh(geometry, material);
            const [x, y] = Array(2).fill(1).map(() => (Math.random() * 100 - 50) / 2);
            const z = (Math.random() * 100 - 70) / 2;
            star.position.set(x, y, z);
            this.scene.add(star);
        }
        this.update();
    }
    shootingStar() {
        return __awaiter(this, void 0, void 0, function* () {
            let stars = [];
            for (let i = 0; i < 100; i++) {
                let clonesID = [];
                let clearClones = () => __awaiter(this, void 0, void 0, function* () {
                    clonesID.forEach((clone) => {
                        this.scene.getObjectById(clone);
                        this.scene.remove(this.scene.getObjectById(clone));
                    });
                });
                const geometry = new THREE.SphereGeometry(0.3, 23, 23);
                const material = new THREE.MeshBasicMaterial({ color: 0xF99B2A });
                const star = new THREE.Mesh(geometry, material);
                let [x, y] = Array(2).fill(1).map(() => (Math.random() * 100 - 50) / 2);
                let z = (Math.random() * 100 - 10) / 2 - 50;
                star.position.set(x, y, z);
                let tx = Math.random() / 30 - 1 / 60;
                let ty = Math.random() / 30 - 1 / 60;
                let clones = [];
                let ferq = 100;
                for (let i = 0; i < ferq; i++) {
                    clones.push(star.clone());
                    clonesID.push(clones[i].id);
                    clones[i].position.x -= (i / ferq) * 100 * tx;
                    clones[i].position.y -= (i / ferq) * 100 * ty;
                    clones[i].scale.set(0.75 - 0.75 * i / ferq, 0.75 - 0.75 * i / ferq, 0.75 - 0.75 * i / ferq);
                }
                stars.push([
                    star,
                    clones,
                    tx,
                    ty,
                    clonesID
                ]);
            }
            let mooving = (id) => {
                let star = stars[id][0];
                let clones = stars[id][1];
                let tx = stars[id][2];
                let ty = stars[id][3];
                let clonesID = stars[id][4];
                star.position.x += tx;
                star.position.y += ty;
                clones.forEach((clone) => {
                    clone.position.x += tx;
                    clone.position.y += ty;
                });
            };
            let animate = (id) => {
                requestAnimationFrame(animate);
                mooving(id);
                this.update;
            };
            let ID = 0;
            let int = setInterval(() => {
                this.scene.add(stars[ID][0]);
                stars[ID][1].forEach((clone) => {
                    this.scene.add(clone);
                });
                for (let i = 0; i < 500; i++) {
                    animate(ID);
                }
                ID++;
                if (ID > stars.length) {
                    clearInterval(int);
                }
            }, 3000);
        });
    }
    update() {
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.render(this.scene, this.camera);
    }
    spaceShip() {
        let createShip = () => {
            const geometry = new THREE.ConeGeometry(2.5, 8, 3, 90, false);
            const material = new THREE.MeshStandardMaterial({
                color: 0x334455,
                roughness: 0.2,
                metalness: 0.6
            });
            material.normalMap = this.textures.metal;
            const ship = new THREE.Mesh(geometry, material);
            return ship;
        };
        let initShip = (ship) => {
            ship.position.x = 0;
            ship.position.y = 10;
            ship.position.z = -50;
            ship.up = new THREE.Vector3(0, 5, 0);
            ship.rotateZ(Math.PI / 2 + Math.PI);
            this.scene.add(ship);
            this.update();
        };
        let mouseX = 0;
        let mouseY = 0;
        let rotationSpeed = 0.005;
        let ascentRate = 0;
        setInterval(() => {
            ascentRate = (Math.random() - 0.5) * 0.1;
        }, 3000);
        let animateShip = (ship, delta) => {
            ship.rotateX(-1 * rotationSpeed);
            ship.position.x = (100 * Math.sin(delta) + 0);
            ship.position.y += ascentRate;
        };
        let mouseRefresh = (e) => {
            mouseX = e.clientX;
            mouseY = e.clientY;
        };
        window.addEventListener('mousemove', mouseRefresh);
        const ship = createShip();
        initShip(ship);
        let gt = 0;
        let animation = () => {
            requestAnimationFrame(animation);
            gt += rotationSpeed;
            animateShip(ship, gt);
            this.update();
        };
        animation();
    }
}
//# sourceMappingURL=background.js.map